#ifndef COLONNE_H
#define COLONNE_H

#include <vector>
#include <string>

#include "case.h"

class Colonne{

public:
    //#Constructor
    Colonne(Case* caseHaute,Case* caseMilieu, Case* caseBas);
    Colonne(std::vector<Case*> allCase,int caseAMettre,int longueurLigne,int index);
    //#Getter
    Case GetCaseParIndexe(int i);
    //#Methode
    std::string ToString(){
        std::string tmp = "";
        for(Case* i : m_colonneCase) tmp = tmp + i->ToString() + "\n";
        return tmp;
    }
    bool EstElleComplete();
    bool EstElleComplete(int idJoueur);
    std::vector<Case*> m_colonneCase;
private :

};

#endif // COLONNE_H
