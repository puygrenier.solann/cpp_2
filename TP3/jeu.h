#ifndef JEU_H
#define JEU_H

#include <string>
#include <iostream>
#include "grillemorpion.h"
#include "grillepuissance4.h"

class Jeu{
public:
    //#Attribut
    //#Constructeurs
    Jeu(GrilleMorpion* grilleMorpion, GrillePuissance4* grillpuiss4);
    //#Methodes
    //  -typeDeJeu a mettre parmi : "Morpion" et "Puissance 4"
    void StartGame(std::string typeDeJeu);

    std::string RecordInput(std::string cpuRequest);
    bool ControleInputLigneOkMorpion(std::string input);
    bool ControleInputColonneOkMorpion(std::string input);
    bool ControleInputLigneOkPuiss4(std::string input);
    bool ControleInputColonneOkPuiss4(std::string input);
    //  -typeDeJeu a mettre parmi : "Morpion" et "Puissance 4"
    Case* AskCase(std::string typeDeJeu);
    void PlacePion(Case* theCase,int idJoueur);

    bool ControleCaseVideMorpion(int ligne,int colonne);
    bool ControleCaseVidePuiss4(int ligne);
    Case* CasePuissanceQuatre(int colonne);
    inline void CleanGame(){Case::InitialiseToutesLesCaseA(0);}
private:
    GrilleMorpion* m_grilleMorpion;
    GrillePuissance4* m_grillePuissance4;
};

#endif // JEU_H
