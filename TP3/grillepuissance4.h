#ifndef GRILLEPUISSANCE4_H
#define GRILLEPUISSANCE4_H

#include <vector>
#include "colonne.h"
#include "ligne.h"
#include "diagonale.h"

class GrillePuissance4
{
public:
    //#Attribut
    std::vector<Colonne*> m_colonnes;
    std::vector<Ligne*> m_lignes;
    std::vector<Diagonale*> m_diagonales;
    //#Constructeur
    GrillePuissance4(std::vector<Ligne*> vectorOfLignes,std::vector<Colonne*> vectorOfColonnes,std::vector<Diagonale*> vectorOfDiagonales);
    //#Methode
    void Affiche();
    std::string ToString();
    bool LigneComplete(Ligne lg,int idJoueur);
    bool ColonneComplete(Colonne col,int idJoueur);
    bool ColonnePleine(Colonne col,int idJoueur);
    bool DiagonaleComplete(Diagonale dg,int idJoueur);
    void DeposerJeton(Case* aCase,int idJoueur);
    bool VictoireJoueur(int idJoueur);

};

#endif // GRILLEPUISSANCE4_H
