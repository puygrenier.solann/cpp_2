TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        case.cpp \
        colonne.cpp \
        diagonale.cpp \
        grillemorpion.cpp \
        grillepuissance4.cpp \
        jeu.cpp \
        ligne.cpp \
        main.cpp \
        pion.cpp

HEADERS += \
    case.h \
    colonne.h \
    diagonale.h \
    grillemorpion.h \
    grillepuissance4.h \
    jeu.h \
    ligne.h \
    pion.h
