#include "jeu.h"

Jeu::Jeu(GrilleMorpion* grilleMorpion, GrillePuissance4* grillpuiss4):m_grilleMorpion(grilleMorpion),m_grillePuissance4(grillpuiss4){}

void Jeu::StartGame(std::string typeDeJeu){
    bool stayloop = true;
    bool changeTour = true;
    int idJoueur;
    int nombreDePion =0;
    if(typeDeJeu == "Morpion"){
        while(stayloop){
            nombreDePion++;
            system("cls");
            m_grilleMorpion->Affiche();
            if(changeTour){
                idJoueur = 1;
                changeTour = !changeTour;
            }
            else {
                idJoueur = 2;
                changeTour = !changeTour;
            }
            Case* caseChoisie = this->AskCase(typeDeJeu);
            this->PlacePion(caseChoisie,idJoueur);

            if(m_grilleMorpion->VictoireJoueur(idJoueur)){
                system("cls");
                m_grilleMorpion->Affiche();
                std::cout << "JOUEUR " << idJoueur << " GAGNE" << std::endl;
                stayloop = false;
            }
            if(nombreDePion == 9){
                system("cls");
                m_grilleMorpion->Affiche();
                std::cout << "Grille pleine ... Match nul"<< std::endl;
                stayloop = false;
            }
        }
    }
    else if(typeDeJeu == "Puissance 4"){
        while(stayloop){
            nombreDePion++;
            system("cls");
            m_grillePuissance4->Affiche();
            if(changeTour){
                idJoueur = 1;
                changeTour = !changeTour;
            }
            else {
                idJoueur = 2;
                changeTour = !changeTour;
            }
            Case* caseChoisie = this->AskCase(typeDeJeu);
            this->PlacePion(caseChoisie,idJoueur);

            if(m_grillePuissance4->VictoireJoueur(idJoueur)){
                system("cls");
                m_grillePuissance4->Affiche();
                std::cout << "JOUEUR " << idJoueur << " GAGNE" << std::endl;
                stayloop = false;
            }
            if(nombreDePion == 28){
                system("cls");
                m_grillePuissance4->Affiche();
                std::cout << "Grille pleine ... Match nul"<< std::endl;
                stayloop = false;
            }
        }
    }
}

std::string Jeu::RecordInput(std::string cpuRequest){
    std::string UserAnswer;
    std::cout << cpuRequest << std::endl;
    std::cout << "->" ;
    std::cin >> UserAnswer;
    return UserAnswer;
}
bool Jeu::ControleInputLigneOkMorpion(std::string input){
    for (int i = 0; i < input.length(); i++) if (isdigit(input[i]) == false) return false;
    if((std::stoi(input)<=0)||(std::stoi(input)>3))return false;
    else return true;
}
bool Jeu::ControleInputColonneOkMorpion(std::string input){
    for (int i = 0; i < input.length(); i++) if (isdigit(input[i]) == false) return false;
    if((std::stoi(input)<=0)||(std::stoi(input)>3))return false;
    else return true;
}
bool Jeu::ControleInputLigneOkPuiss4(std::string input){
    for (int i = 0; i < input.length(); i++) if (isdigit(input[i]) == false) return false;
    if((std::stoi(input)<=0)||(std::stoi(input)>4))return false;
    else return true;
}
bool Jeu::ControleInputColonneOkPuiss4(std::string input){
    for (int i = 0; i < input.length(); i++) if (isdigit(input[i]) == false) return false;
    if((std::stoi(input)<=0)||(std::stoi(input)>7))return false;
    else return true;
}
Case* Jeu::AskCase(std::string typeDeJeu){
    bool stayloop = true;
    std::string stringColonne;
    std::string stringLigne;
    int colonne;
    int ligne;
    if(typeDeJeu == "Morpion"){
        while(stayloop){
            stringLigne = RecordInput("Merci de rentrer le no de ligne");
            stringColonne = RecordInput("Merci de rentrer le no de colonne");
            if(ControleInputColonneOkMorpion(stringColonne)&&ControleInputLigneOkMorpion(stringLigne)){
                colonne= std::stoi(stringColonne);
                ligne = std::stoi(stringLigne);
                if(ControleCaseVideMorpion(ligne,colonne))stayloop = false;
                else std::cout << "[!] CASE DEJA PLEINE [!]" <<std::endl;
                }
            else std::cout << "[!] VALEUR(S) NON CONFORME [!]" <<std::endl;
        }
        return m_grilleMorpion->m_lignes[ligne-1]->m_ligneCase[colonne-1];
    }
    else if(typeDeJeu == "Puissance 4"){
        while(stayloop){
            stringColonne = RecordInput("Merci de rentrer le no de colonne");
            if(ControleInputColonneOkPuiss4(stringColonne)){
                colonne= std::stoi(stringColonne);
                if(ControleCaseVidePuiss4(colonne))stayloop = false;
                else std::cout << "[!] CASE DEJA PLEINE [!]" <<std::endl;
                }
            else std::cout << "[!] VALEUR(S) NON CONFORME [!]" <<std::endl;
        }
        return CasePuissanceQuatre(colonne);
    }
}
void Jeu::PlacePion(Case* theCase,int idJoueur){
    theCase->ChangePionJoueur(idJoueur);
}

bool Jeu::ControleCaseVideMorpion(int ligne,int colonne){
    Case* tmp = m_grilleMorpion->m_lignes[ligne-1]->m_ligneCase[colonne-1];
    if (tmp->GetPion().GetNumeroDuJoueur() == 0) return true;
    else return false;
}
bool Jeu::ControleCaseVidePuiss4(int colonne){
    for(Case* i : m_grillePuissance4->m_colonnes[colonne-1]->m_colonneCase){
        if (i->GetPion().GetNumeroDuJoueur() == 0) return true;
    }
    return false;
}

Case* Jeu::CasePuissanceQuatre(int colonne){
    Case* tmp;
    for(Case* i : m_grillePuissance4->m_colonnes[colonne-1]->m_colonneCase){
        if (i->GetPion().GetNumeroDuJoueur() ==  0) tmp = i;
        else if(i->GetPion().GetNumeroDuJoueur() !=  0) break;
    }
    return tmp;
}
