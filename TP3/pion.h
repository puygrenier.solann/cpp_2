#ifndef PION_H
#define PION_H
#include <string>

class Pion
{
public:
    //#Constructeur
    Pion();//Pion null : joueur a 0
    //#Getter et Setter
    inline int GetId()const {return m_idDuPion;}
    inline int GetNumeroDuJoueur(){return m_numeroDuJoueur;}
    inline void SetNumeroDuJoueur(int numero){m_numeroDuJoueur = numero;}
    //#Methodes
    inline std::string ToString(){return " " + std::to_string(m_numeroDuJoueur)+ " ";}
private :
    int m_numeroDuJoueur;
    static int m_conteurDePion;
    int m_idDuPion;
    const int m_maximumDePion = 9; //Valeur pour le morpions, fait uniqumeent pour le scope du TP3
};

#endif // PION_H
